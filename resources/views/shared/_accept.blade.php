    @can('accept',$model)
        <a title="Mark as best" class="{{ $model->status }} mt-2"
            onclick="event.preventDefault(); document.getElementById('accept-{{ $name }}-{{ $model->id }}').submit()"
            >
            <i class="fas fa-check fa-2x"></i>
        </a>
        <form id="accept-{{ $name }}-{{ $model->id }}" action="{{ route($name.'.accept',$model->id)}}" method="POST" style="display: none;">@csrf</form>
    @else
        @if ($model->is_best)
        <a title="This {{ $name }} is marked as best" class="{{ $model->status }} mt-2">
            <i class="fas fa-check fa-2x"></i>
        </a>
        @endif
    @endcan
