@if($model instanceof App\Question)
        @php
            $name = 'question';
        @endphp
@elseif($model instanceof App\Answer)
        @php
            $name = 'answer';
        @endphp
@endif
<div class="d-flex flex-column vote-controls">
    <a title="This $name is useful" onclick="event.preventDefault(); document.getElementById('up-vote-{{ $name }}-{{ $model->id }}').submit()" class="vote-up {{ Auth::guest() ? 'off' : '' }}">
        <i class="fas fa-caret-up fa-3x"></i>
    </a>
    <form id="up-vote-{{ $name }}-{{ $model->id }}" action="{{route($name.'.vote',$model->id)}}" method="POST" style="display: none;">
        @csrf
        @if ($model->is_Voted)
        @method('DELETE')
        @endif
        <input type="hidden" name="vote" value="1">
    </form>
    <span class="rounded-circle votes-count">{{ $model->votes_count }}</span>
    <a title="This {{ $name }} is not useful" onclick="event.preventDefault(); document.getElementById('down-vote-{{ $name }}-{{ $model->id }}').submit()" class="vote-down {{ Auth::guest() ? 'off' : '' }}">
        <i class="fas fa-caret-down fa-3x"></i>
    </a>
    <form id="down-vote-{{ $name }}-{{ $model->id }}" action="{{route($name.'.vote',$model->id)}}" method="POST" style="display: none;">
        @csrf
        @if ($model->is_Voted)
        @method('DELETE')
        @endif
        <input type="hidden" name="vote" value="-1">
    </form>
    @if($model instanceof App\Question)

        <favorite :model="{{ $model }}"></favorite>
    @elseif($model instanceof App\Answer)
        <accept :model="{{ $model }}"></accept>
    @endif
</div>
