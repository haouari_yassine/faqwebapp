<a title="Mark as favorite" onclick="event.preventDefault(); document.getElementById('favorite-{{ $name }}-{{ $model->id }}').submit()" class="favorite mt-2 {{ Auth::guest() ? 'off' : ($model->is_favorited ? 'favorited' : '' )}}">
<i class="fas fa-star fa-2x "></i>
</a>
<form id="favorite-{{ $name }}-{{ $model->id }}" action="{{route($name.'.favorite',$model->id)}}" method="POST" style="display: none;">
    @csrf
    @if ($model->is_favorited)
    @method('DELETE')
    @endif
</form>
<span class="favorites-count">{{ $model->favorites_count}} </span>
