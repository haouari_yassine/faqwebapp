<span class="text-mutee mt-2">{{ $label . ' ' . $model->created_date }}</span>
<div class="media mt-2 ml-2 mb-2">
    <a class="pr-2" href="{{$model->user->url }}">
        <img width="{{$model->user->avatarsize }}" height="{{$model->user->avatarsize }}" src="{{$model->user->avatar }}" class="img-fluid" alt="Responsive image">
    </a>
    <div class="media-body">
            <a href="{{$model->user->url }}">
                {{$model->user->name }}</a>
    </div>
</div>
