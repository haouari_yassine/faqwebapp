    @extends('layouts.app')
    @section('content')
    <div class="container">
        <div class="row justify-content-center">
            <question :model='{{ $question }}'></question>
            <answers :model='{{ $question }}'></answers>
        </div>
    </div>
    @endsection
