@extends('layouts.app')

@section('content')
<div class="container col-md-12">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-item-center">
                        <h2>Edit questions</h2>
                        <div class="ml-auto" >
                            <a href="{{ route('questions.index')}}" class="btn btn-outline-secondary">Show all questions</a>
                            </div>
                    </div>

                </div>

                <div class="card-body">


                    <div >

                        <form action="{{ route('questions.update', $question->id)}}" method="POST">
                            {{ method_field('PUT') }}
                            @include('layouts._form', ['btntxt'=> 'Edit this question'])

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
