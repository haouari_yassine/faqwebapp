@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-item-center">
                        <h2>Ask questions</h2>
                        <div class="ml-auto" >
                            <a href="{{ route('questions.index')}}" class="btn btn-outline-secondary">Show all questions</a>
                            </div>
                    </div>

                </div>

                <div class="card-body">


                    <div >

                        <form action="{{ route('questions.store')}}" method="POST">
                            @include('layouts._form', ['btntxt'=> 'Ask this new question'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
