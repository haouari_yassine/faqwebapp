@auth
    <div class="col-md-12">
        <div class="card mt-4">
            <div class="card-body">
                <div class="card-titlr">
                    <div class="d-flex">
                    <h3>Your answer</h3>
                    </div>
                </div><hr>
                <form action="{{ route('questions.answers.store',$question->id)}}" method="post">
                    @csrf
                    <div class="form-group">

                        <textarea name="body" id="question-body" rows="10" class="form-control {{ $errors->has('body') ? 'is-invalid' : ''}}" placeholder="Explaine"></textarea>
                        @if ($errors->has('body'))
                      <div class="invalid-feedback">
                          <strong>{{ $errors->first('body')}}</strong>
                      </div>
                  @endif
                    </div>
                    <div class="form-group">
                      <input type="submit" name="" id="" class="btn btn-lg btn-outline-primary" title="Submite" placeholder="" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endauth
@guest
    <div class="col-md-12">
        <div class="card mt-4">
            <div class="card-body">
                <div class="card-titlr">
                    <div class="d-flex">
                        <h3>Your answer</h3>
                    </div>
                </div>
                <hr>
                <a href="{{ route('login')}}">Login to post an answer</a>
            </div>
        </div>
    </div>
@endguest
