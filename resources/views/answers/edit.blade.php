@extends('layouts.app')

@section('content')
<div class="container">
<div class="row mt-3">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

            <div class="card-titlr">
                <h2>Editing answer for question : <strong>{{ $question->title}}</strong></h2>

            </div>
                @auth
                    <hr>
                    <form action="{{ route('questions.answers.update',[$question->id, $answer->id])}}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">

                            <textarea name="body" id="question-body" rows="10" class="form-control {{ $errors->has('body') ? 'is-invalid' : ''}}" placeholder="Explaine">{{ old('body',$answer->body)}}</textarea>
                            @if ($errors->has('body'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('body')}}</strong>
                        </div>
                    @endif
                        </div>
                        <div class="form-group">
                        <input type="submit" name="" id="" class="btn btn-lg btn-outline-primary" title="Update" placeholder="" aria-describedby="helpId">
                        </div>
                    </form>
                @endauth
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
