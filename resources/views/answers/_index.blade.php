@if ($answersCount > 0)
    <div class="col-md-12" v-cloak>
        <div class="card mt-4">

            <div class="card-body">
                <div class="card-titlr">
                    <div class="d-flex">
                        <h3 class="info">{{$answersCount . ' ' . Str::plural('Answer',$answersCount ) }}</h3>
                    </div>
                </div>
                <hr>
                @include('layouts._messages')
                @forelse($answers as $answer)
                @include('answers._answer',['answer' => $answer])
                @empty
                    <div class="alert alert-danger">
                        <strong>Sorry!</strong> there is no answer yet
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endif


