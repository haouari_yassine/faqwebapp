<answer :model='{{ $answer }}' inline-template>
    <div class="media post">
        <vote :model='{{ $answer }}' name='answer'></vote>

        <div class="media-body">
            <form v-if="editing" @submit.prevent="answerupdate" >
                <div class="form-group">
                  <textarea required v-model="body" class="form-control {{ $errors->has('body') ? 'is-invalid' : ''}}" placeholder="Explaine" name="" id="" rows="10"></textarea>
                </div>
                <button type="submit" :disabled="isInvalid"  class="btn btn-outline-primary">Edit</button>
                <button type="button" @click="answercancel"  class="btn btn-outline-secondary">Cancel</button>
            </form>
            <div v-if="!editing">

                    <div v-html="bodyHtml"></div>

                    <div class="row">
                        <div class="col-4">
                            <div class="ml-auto"><!-- href="{{ route('questions.answers.edit',[$question->id,$answer->id]) }}" -->
                                @can('update',$answer)
                                    <a @click.prevend="answeredit"  data-loading-text=”Deleting…”  class="btn btn-sm btn-outline-info" >Edit</a>
                                @endcan
                                @can('delete',$answer)
                                        <button @click.prevend="answerdelete" data-loading-text=”Deleting…”  class="btn btn-sm btn-outline-danger" >Delete</button>
                                @endcan
                            </div>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-4">
                            <user-info :model='{{ $answer }}' label='Answered  '></user-info>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</answer>
