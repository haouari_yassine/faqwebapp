export default {
    modify: function (user , model) {
        return user.id == model.user_id;
    },
    accept: function (user , model) {
        return user.id == model.question.user_id;
    },
    deletequestionpolicy: function (user , model) {
        return user.id == model.user_id && model.answers_count <1 ;
    },
}
