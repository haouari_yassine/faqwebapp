<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','QuestionsController@index');

Route::resource('questions', 'QuestionsController')->except('show');
Route::get('questions/{slug}', 'QuestionsController@show')->name('questions.show');
Route::resource('questions.answers', 'AnswersController')->except('create','show');
Auth::routes();
Route::post('/answers/{answer}/accept','AcceptAnswerController')->name('answer.accept');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/questions/{question}/favorites','FavoritesController@store')->name('question.favorite');
Route::delete('/questions/{question}/favorites','FavoritesController@destroy')->name('question.unfavorite');
Route::post('/questions/{question}/vote','VoteQuestionController')->name('question.vote');
Route::delete('/questions/{question}/vote','VoteQuestionController')->name('question.vote');
Route::post('/answers/{answer}/vote','VoteAnswerController')->name('answer.vote');
Route::delete('/answers/{answer}/vote','VoteAnswerController')->name('answer.vote');
