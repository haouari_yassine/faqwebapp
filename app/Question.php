<?php
/** Question
 * The questions class
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;
use App\Answer;
use App\Http\Controllers\AcceptAnswerController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Question extends Model
{
    use HasFactory;

    use VotableTrait;
    protected $fillable = ['title', 'body'];
    protected $appends = ['body_html','created_date','favorites_count','is_favorited'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = str::slug($title);
    }
    public function getUrlAttribute()
    {
        return Route('questions.show', $this->slug);
    }
/*    public function getVotesCountAttribute()
    {
        return $this->votes_count;
    }*/
    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffforhumans();
    }
    public function getStatusAttribute()
    {
        if ($this->answers_count > 0) {
            if ($this->best_answer_id) {
                return 'answered-accepted';
            }
            return 'answered';
        }
        return 'unaswered';
    }

    /**
     *
     *  This can convert body stored in db to HTML format
     *
     */
    public function getBodyHtmlAttribute()
    {
        //return clean($this->bodyHtml());
        return $this->bodyHtml();
    }
    /**
     * Answer relationship
     */
    public function answers()
    {
        return $this->hasMany(Answer::class)->orderBy('votes_count','DESC') ;
    }
    public function acceptBestAnswer($answer)
    {
        $this->best_answer_id=$answer->id;
        $this->save();
    }
    public function favorites()
    {
        return $this->belongsToMany(User::class,'favorites')->withTimestamps();
    }
    public function isFavorited()
    {
        return $this->favorites()->where('user_id', auth()->id())->count() > 0;
    }
    public function getIsFavoritedAttribute()
    {
        return $this->isFavorited();
    }
    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }
    public function getExcerptAttribute()
    {
        return $this->excerpt(254);
    }
    public function excerpt($var)
    {
        return Str::limit(strip_tags($this->bodyHtml()),$var );
        # code...
    }
    protected function bodyHtml()
    {
        return \Parsedown::instance()
            ->setBreaksEnabled(true)
            ->text($this->body, 'breaks_enabled');
    }
}
