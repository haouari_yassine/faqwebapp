<?php
/** User
 * The uders class
 */
namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class User extends Authenticatable
{
    use Notifiable;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     *
     * */
    protected $appends = ['url', 'avatar','avatarsize'];


    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * HasMany
     * Relationship with question model
     *
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }
    public function getUrlAttribute()
    {
        return '#';
        // return Route('questions.show',$this->id);
    }
    /**
     *
     * Answer relationship
     */
    public function answers()
    {
        $this->hasMany(Ansewr::class);
    }
    public function getAvatarAttribute()
    {
        $email = $this->email;
        $default = "avatar.png";
        $size = 100;
        $grav_url = "http://evatar.io/" . md5( strtolower( trim( $email ) ) ) . "&s=" . $size . "?d=" . urlencode( $default );
        return $grav_url;
    }
    public function getAvatarsizeAttribute()
    {
        $size = 24;
        return $size;
    }
    public function favorites()
    {
        return $this->belongsToMany(Question::class,'favorites')->withTimestamps();
    }
    public function voteQuestions()
    {
        return $this->morphedByMany(Question::class,'votable')->withTimestamps();
    }
    public function voteAnswers()
    {
        return $this->morphedByMany(Answer::class,'votable')->withTimestamps();
    }
    public function voteQuestion(Question $question , $vote)
    {
        $voteQuestions = $this->voteQuestions();
        return $this->_vote($voteQuestions,$question,$vote);
    }
    public function voteAnswer(Answer $answer ,$vote)
    {
        $voteAnswers = $this->voteAnswers();
        return $this->_vote($voteAnswers,$answer,$vote);
    }
    private function _vote($relationship,$model,$vote)
    {
        try {
            if ($relationship->where('votable_id',$model->id)->exists()) {
                $relationship->updateExistingPivot($model,['vote'=>$vote]);
            }else{
                $relationship->attach($model,['vote'=>$vote]);
            }
            $model->load('votes');
            $downVotes = (float) $model->votes()->wherePivot('vote', -1 )->sum('vote');
            $upVotes = (float) $model->votes()->wherePivot('vote', 1 )->sum('vote');
            $model->votes_count = (int) $downVotes + $upVotes;
            $model->save();
        } catch (\Throwable $th) {
        //throw $th;
            $relationship->detach($model,['vote'=>$vote]);
            $model->load('votes');
            $downVotes = (float) $model->votes()->wherePivot('vote', -1 )->sum('vote');
            $upVotes = (float) $model->votes()->wherePivot('vote', 1 )->sum('vote');
            $model->votes_count = (int) $downVotes + $upVotes;
            $model->save();
        }
        return $model->votes_count;
    }
}
