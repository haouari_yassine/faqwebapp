<?php
namespace App;
trait VotableTrait
{
    public function votes()
    {
        return $this->morphToMany(User::class,'votable');
    }
    public function upVotes()
    {
        return $this->votes()->wherePivot('vote', 1 )->sum('vote');
    }
    public function downVotes()
    {
        return $this->votes()->wherePivot('vote', -1 )->sum('vote');
    }
    public function isVoted()
    {
        return $this->votes()->where('user_id', auth()->id())->count() > 0;
    }
    public function getIsVotedAttribute()
    {
        return $this->isVoted();
    }
}

