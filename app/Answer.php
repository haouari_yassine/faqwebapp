<?php
/** Answer
 * The answers class
 */
namespace App;
use App\Question;
use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\Model;
/** Answer
 * The answers class
 */
class Answer extends Model
{
    use HasFactory;

    use VotableTrait;
    protected $fillable = ['user_id', 'body'];
    protected $appends = ['created_date','body_html','is_best'];

    /** BelongsTO
    * Relationship with question model
    */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }
    /** BelongsTO
    * Relationship with user model
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     *
     *  This can convert body stored in db to HTML format
     *
     */
    public function getBodyHtmlAttribute()
    {
        return \Parsedown::instance()
            ->setBreaksEnabled(true)
            ->text($this->body, 'breaks_enabled');
    }
    /**
     * The answer will increment the vale ofanswer_count
     * in every call of the this class
     * that's when an answer is created
     */
    public static function boot()
    {
        parent::boot();
        static::created(function ($answer)
        {
            $id = $answer->question_id;
            \App\Question::findOrFail($id)->increment("answers_count");
        });
        static::deleted(function ($answer)
        {
            // question->decrement('answers_count');
            Question::findOrFail($answer->question_id)->decrement("answers_count");
            $question = Question::findOrFail($answer->question_id);
            if ($question->best_answer_id == $answer->id) {
                $question->best_answer_id = 0;
                $question->save();
            }
        });
    }
    public function isBest()
    {
        return $this->id == $this->question->best_answer_id;
    }
    public function getStatusAttribute($answer)
    {
        return $this->isBest() ? 'vote-accept' : '';
    }
    public function getIsBestAttribute($answer)
    {
        return $this->isBest();
    }
    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffforhumans();
    }

}
