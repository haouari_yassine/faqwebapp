<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
class VoteQuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function __invoke(Request $request ,Question $question)
    {
        //dd('Accepting answer in progress...');
        //$this->authorize('accept' , $answer);

        //$question->save();
        $vote = (float) request()->vote;
        $votesCount = auth()->user()->voteQuestion($question,$vote);
        if ($request->expectsJson()) {
            # code...
            $data = [ 'message' => 'Question voted',
                        'votesCount' => $votesCount];
            return response()->json($data);
        };
        return back()->with('success2','Question voted ');
    }
}
