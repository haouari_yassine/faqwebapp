<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;

class VoteAnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function __invoke(Request $request ,Answer $answer)
    {
        //dd('Accepting answer in progress...');
        //$this->authorize('accept' , $answer);

        //$question->save();
        $vote = (float) request()->vote;
        $votesCount = auth()->user()->voteAnswer($answer,$vote);
        if ($request->expectsJson()) {
            # code...
            $data = [ 'message' => 'Answer voted',
            'votesCount' => $votesCount];
            return response()->json($data);
        };
        return back()->with('success','Answer voted ');
    }
}
