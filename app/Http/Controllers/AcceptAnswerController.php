<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Question;
class AcceptAnswerController extends Controller
{
    public function __invoke(Answer $answer)
    {
        //dd('Accepting answer in progress...');
        $this->authorize('accept' , $answer);

        $answer->question->acceptBestAnswer($answer);
        //$question->save();

        if (request()->expectsJson()) {
            # code...
            $data = [ 'message' => 'This answer has been accepted as best'];
            return response()->json($data);
        }
        return back()->with('success','Answer matched as best');
    }
}
