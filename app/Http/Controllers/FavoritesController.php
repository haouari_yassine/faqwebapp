<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class FavoritesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request ,Question $question)
    {
        $question->favorites()->attach(auth()->id());
        if ($request->expectsJson()) {
            # code...
            $data = [ 'message' => 'The question is marked as favorite'];
            return response()->json($data);
        }
        return back()->with('success2','Your question has been added to favorites');

    }
    public function destroy(Request $request ,Question $question)
    {
        $question->favorites()->detach(auth()->id());
        if ($request->expectsJson()) {
            # code...
            $data = [ 'message' => 'The question is marked as favorite'];
            return response()->json($data);
        }
        return back()->with('success2','Your question has been removed from favorites');

    }
}
