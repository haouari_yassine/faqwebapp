<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Question;
use App\User;
use App\Answer;

class UsersQuestionsAnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('answers')->delete();
        \DB::table('questions')->delete();
        \DB::table('users')->delete();
        \App\User::factory()->count(30)->create()->each(function($user = null)
            {
            $user->questions()->saveMany(
                
                \App\Question::factory(rand(4,9))->make())
                ->each(function($question = null)
                {
                    $question->answers()->saveMany(
                        \App\Answer::factory(rand(5,7))->make());
                }
            );
            }
        );
    }
}
